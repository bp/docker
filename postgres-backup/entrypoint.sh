#!/usr/bin/env sh

set -e

TIME="$(date +%s)"
PGPASSWORD="${POSTGRES_PASSWORD}"
echo "${GCP_KEY}" > key.json

gcloud auth activate-service-account --key-file=./key.json
pg_dump -U "${POSTGRES_USER}" -h "${POSTGRES_HOST}" -p "${POSTGRES_PORT}" -F t "${POSTGRES_DB}" > "${TIME}.tar"
gsutil cp "${TIME}.tar" "gs://${GCP_BACKUP_BUCKET}"
